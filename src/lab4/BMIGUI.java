package lab4;

//********************************************************************
//BMIGUI.java     
//
//Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BMIGUI
{
private int WIDTH = 300;
private int HEIGHT = 120;

private JFrame frame;
private JPanel panel;
private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel, CommentLabel;
private JTextField wzrost, waga;
private JButton oblicz;

//-----------------------------------------------------------------
//  Ustawia GUI.
//-----------------------------------------------------------------
public BMIGUI()
{
  frame = new JFrame ("Kalkulator BMI");
  frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

  //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
  wzrostLabel = new JLabel ("Twoj wzrost w centymetrach:");
  wagaLabel = new JLabel ("Twoja waga w kilokramach: ");
  BMILabel = new JLabel("to jsest twoje BMI");
  wynikLabel = new JLabel("");
  CommentLabel=new JLabel("		");

  wzrost=new JTextField("0000");
  waga=new JTextField("0000");

  oblicz=new JButton("Oblicz");
  oblicz.addActionListener(new BMIListener());
  

  // ustawienia JPanel znajdujacego sie na JFrame 
  panel = new JPanel();
  panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
  panel.setBackground (Color.yellow);

  panel.add(wzrostLabel);
  panel.add(wzrost);
  
  panel.add(wagaLabel);
  panel.add(waga);

  panel.add(oblicz);
  panel.add(BMILabel);
  panel.add(wynikLabel);
  panel.add(CommentLabel);

  //dodaj panel do frame 
  
  frame.getContentPane().add(panel);
}

//-----------------------------------------------------------------
//  Wyswietl frame aplikacji podstawowej
//-----------------------------------------------------------------
public void display()
{
  frame.pack();
  frame.show();
}

//*****************************************************************
//  Reprezentuje action listenera dla przycisku oblicz.
//*****************************************************************
private class BMIListener implements ActionListener
{
  //--------------------------------------------------------------
  //  Wyznacz BMI po wcisnieciu przycisku
  //--------------------------------------------------------------
  public void actionPerformed (ActionEvent event)
  {
     String wzrostText, wagaText;
     int wagaVal;
     double bmi,wzrostVal;
 	wzrostText=wzrost.getText();
 	wagaText=waga.getText();
 	
 	wzrostVal=0.01*Double.parseDouble(wzrostText);
 	wagaVal=Integer.parseInt(wagaText);

 	bmi=wagaVal/(wzrostVal*wzrostVal);
 
 	wynikLabel.setText(Double.toString(bmi));

 	if(bmi<19)
 	{
 		CommentLabel.setText("Niedowaga");
 	}
 	else if(bmi<=25)
 	{
 		CommentLabel.setText("Waga jest OK");
 	}
 	else if(bmi<=30)
 	{
 		CommentLabel.setText("Nadwaga");
 	}
 	else
 	{
 		CommentLabel.setText("Otylosc");
 	}
 // Wykorzystaj Double.toString do konwersji double na string.

  }
}
}

